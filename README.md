# Introduction
Personal notes of things that I learn!

## Read
- A html version of all notes are also available for better readibility.

## Contents
- A base knowledge of C
- LPIC 1

# Software
All of notes are made by vimwiki ‒ a extention to vim.

# Contrubute
Fell free to read my notes and recommend me cool things!

# LICENSE 
Of course it's GNU General Public License v.3
